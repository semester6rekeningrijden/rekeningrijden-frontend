export enum RecurringType {
    NONE,
    DAILY,
    WORKDAYS,
    WEEKENDS,
    WEEKLY,
    MONTHLY,
    YEARLY
}

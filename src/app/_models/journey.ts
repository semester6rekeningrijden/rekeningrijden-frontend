import {Vehicle} from './vehicle';
import {JourneyData} from './journeyData';

export class Journey {
    id: number = 0;
    finished: boolean;
    vehicle: Vehicle;
    journeyData: Array<JourneyData>
}
import {Role} from './role';

export class User {
    id: number = 0;
    firstName: string = "";
    lastName: string = "";
    password: string = "";
    email: string = "";
    street: string = "";
    city: string = "";
    zipcode: string = "";
    country: string = "";
    role: Role;
}
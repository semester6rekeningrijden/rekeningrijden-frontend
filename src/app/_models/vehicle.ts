import {VehicleType} from './vehicleType';
import {Journey} from './journey';

export class Vehicle {
    id: number = 0;
    licensePlate: string = "";
    type: string = undefined;
    journeys: Array<Journey>;
}
import {Invoice} from './invoice';

export class InvoiceItem {
    public id: number;
    public name: string;
    public description: string;
    public price: number;
    public invoice: Invoice;

}
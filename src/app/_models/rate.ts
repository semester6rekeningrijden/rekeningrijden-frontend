import {RoadType} from './roadType';
import {VehicleType} from './vehicleType';
import {RecurringType} from './recurringType';

export class Rate {
    id: number = 0;
    price: number = 0.00;
    startDate: Date = undefined;
    endDate: Date = undefined;
    vehicleType: VehicleType = VehicleType.ALL;
    roadType: RoadType = RoadType.ALL;
    recurring: RecurringType = RecurringType.NONE;
}
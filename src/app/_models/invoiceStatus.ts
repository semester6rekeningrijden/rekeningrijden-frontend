export enum InvoiceStatus {
    OPEN,
    PAYED,
    CANCELED,
}

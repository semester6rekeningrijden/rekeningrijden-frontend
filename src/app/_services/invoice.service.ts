import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { User } from '../_models';
import {Authentication_V1_API} from '../_helpers/api-constants';
import {Role} from '../_models/role';
import {Invoice} from '../_models/invoice';

@Injectable({ providedIn: 'root' })
export class InvoiceService {
    constructor(private http: HttpClient) { }

    getAll() {
        return this.http.get<Invoice[]>(Authentication_V1_API("invoices"));
    }

    getById(id: number) {
        return this.http.get<Invoice>(Authentication_V1_API("invoices/" + id));
    }

    create(invoice: Invoice) {
        return this.http.post<Invoice>(Authentication_V1_API("invoices"), invoice);
    }

    update(invoice: Invoice) {
        return this.http.patch<Invoice>(Authentication_V1_API("invoices/" + invoice.id), invoice);
    }

    delete(invoice: Invoice) {
        return this.http.delete(Authentication_V1_API("invoices/" + invoice.id));
    }
}

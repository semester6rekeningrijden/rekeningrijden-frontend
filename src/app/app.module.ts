import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { ReactiveFormsModule }    from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

import { AppComponent }  from './app.component';
import { routing }        from './app.routing';

import { JwtInterceptor, ErrorInterceptor } from './_helpers';
import { HomeComponent } from './pages/home';
import { LoginComponent } from './pages/auth/login';
import {AuthGuard} from './_guards';
import {AlertService} from './_services/alert.service';
import {AuthenticationService, CountryService, UserService} from './_services';
import {AlertModule} from './_directives/alert/alert.module';
import {RoleService} from './_services/role.service';
import {PermissionsService} from './_services/permissions.service';
import {RouterModule} from '@angular/router';
import {InvoiceService} from './_services/invoice.service';
import {RateService} from './_services/rate.service';
import {VehicleService} from './_services/vehicle.service';

@NgModule({
    imports: [
        BrowserModule,
        ReactiveFormsModule,
        HttpClientModule,
        AlertModule,
        routing,
        RouterModule,
    ],
    declarations: [
        AppComponent,
        HomeComponent
    ],
    providers: [
        AuthGuard,
        AlertService,
        AuthenticationService,
        UserService,
        RoleService,
        CountryService,
        PermissionsService,
        InvoiceService,
        RateService,
        VehicleService,
        { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
        { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },
    ],
    bootstrap: [AppComponent]
})

export class AppModule { }
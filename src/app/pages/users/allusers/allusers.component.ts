import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {UserService} from '../../../_services';
import {AlertService} from '../../../_services/alert.service';
import {Observable} from 'rxjs/internal/Observable';
import {User} from '../../../_models';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-allusers',
  templateUrl: './allusers.component.html',
  styleUrls: ['./allusers.component.css']
})
export class AllusersComponent implements OnInit {
  private users: User[];
  private dtOptions: DataTables.Settings = {};

    // // We use this trigger because fetching the list of persons can be quite long,
    // // thus we ensure the data is fetched before rendering
  private dtTrigger = new Subject();

  constructor(
      private router: Router,
      private userService: UserService,
      private alertService: AlertService
  ) { }

  ngOnInit() {
      this.dtOptions = {
          pagingType: 'full_numbers',
          pageLength: 10
      };

    this.userService.getAll().forEach((users) => {
      this.users = users;
      this.dtTrigger.next();
    }).catch((error) => {
       this.alertService.error(error.error);
    });
  }
}

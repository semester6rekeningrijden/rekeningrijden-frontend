import { Component, OnInit } from '@angular/core';
import {CountryService, UserService} from '../../../_services';
import {AlertService} from '../../../_services/alert.service';
import {ActivatedRoute, Router} from '@angular/router';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {User} from '../../../_models';
import {Observable} from 'rxjs/internal/Observable';
import {Role} from '../../../_models/role';
import {RoleService} from '../../../_services/role.service';

@Component({
  selector: 'app-updateusers',
  templateUrl: './assignrole.component.html',
  styleUrls: ['./assignrole.component.css']
})
export class AssignroleComponent implements OnInit {
    registerForm: FormGroup;
    loading = false;
    submitted = false;
    
    private userId: number;
    private user: Observable<User>;
    private roles: Observable<Role[]>

  constructor(
      private activatedRoute: ActivatedRoute,
      private formBuilder: FormBuilder,
      private router: Router,
    private userService: UserService,
      private roleService: RoleService,
      private countryService: CountryService,
      private alertService: AlertService
  ) { }

  ngOnInit() {
    this.activatedRoute.paramMap.forEach((params) => {
      this.userId = parseInt(params.get("id"));
    }).catch((error) => {
      this.alertService.error(error.error);
      this.router.navigate(["/users"]);
    });

    this.user = this.userService.getById(this.userId);
    this.roles = this.roleService.getAll();

      this.registerForm = this.formBuilder.group({
          userId: ['', Validators.required],
          roleId: ['', Validators.required],
      });

      this.user.forEach((user) => {
          this.registerForm = this.formBuilder.group({
              userId: [user.id, Validators.required],
              roleId: [user.role.id, Validators.required],
          });
      }).catch((error) => {
          this.alertService.error(error.error);
          this.router.navigate(["/users"]);
      });
  }

    // convenience getter for easy access to form fields
    get f() { return this.registerForm.controls; }

    onSubmit() {
        this.submitted = true;

        if(this.registerForm.invalid) {
            return;
        }

        this.loading = true;
        this.userService.changeRole(this.f.userId.value, this.f.roleId.value).forEach((data) => {
            this.alertService.success('User Updated', true);
            this.router.navigate(['/users']);
        }).catch((err) => {
            this.alertService.error(err.error);
            this.loading = false;
        });
    }

}

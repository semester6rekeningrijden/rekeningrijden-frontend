import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {UserService} from '../../../_services';
import {AlertService} from '../../../_services/alert.service';
import {Observable} from 'rxjs/internal/Observable';
import {User} from '../../../_models';
import { Subject } from 'rxjs';
import {Role} from '../../../_models/role';
import {RoleService} from '../../../_services/role.service';
import {InvoiceService} from '../../../_services/invoice.service';
import {Invoice} from '../../../_models/invoice';

@Component({
  selector: 'app-allinvoices',
  templateUrl: './allinvoices.component.html',
  styleUrls: ['./allinvoices.component.css']
})
export class AllinvoicesComponent implements OnInit {
  private invoices: Invoice[];
  private dtOptions: DataTables.Settings = {};
  showDeleteModal: boolean = false;
  selectedToDelete: Invoice;

    // // We use this trigger because fetching the list of persons can be quite long,
    // // thus we ensure the data is fetched before rendering
  private dtTrigger = new Subject();

  constructor(
      private router: Router,
      private invoiceService: InvoiceService,
      private alertService: AlertService
  ) { }

  ngOnInit() {
      this.dtOptions = {
          pagingType: 'full_numbers',
          pageLength: 10
      };

    this.invoiceService.getAll().forEach((invoices) => {
      this.invoices = invoices;
      this.dtTrigger.next();
    }).catch((error) => {
       this.alertService.error(error.error);
    });
  }

    setDeleteModal(invoice: Invoice){
        this.selectedToDelete = invoice;
        this.showDeleteModal = true;
    }

    deleteRole(invoice: Invoice) {
      this.invoiceService.delete(invoice).forEach((roles) => {
          this.alertService.success("Role " + invoice.id + "has been deleted", true);
          this.router.navigate(["/roles"]);
      }).catch((error) => {
          this.alertService.error(error.error.text, true);
          this.router.navigate(["/roles"]);
      })
    }
}

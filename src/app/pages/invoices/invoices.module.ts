import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {AllinvoicesModule} from './allinvoices/allinvoices.module';
import {InvoicesRoutingModule} from './invoices-routing.module';


@NgModule({
  declarations: [],
  imports: [
      CommonModule,
      InvoicesRoutingModule,
      AllinvoicesModule
  ]
})
export class InvoicesModule { }
